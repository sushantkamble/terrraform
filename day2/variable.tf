variable "ami-id" {
  type = "string"
  default = "ami-0affd4508a5d2481b"
}


variable "instance-type-list" {
    type = "list"
    default = ["t2.micro","t3.small","t3.micro"]  
}