provider "aws" {
  region = "us-east-1"
} 

resource "aws_instance" "webServer" {
 ami = "ami-0affd4508a5d2481b"  
 instance_type = "t2.micro"
 tags = {
  Name = "Sushant007"
  }
  availability_zone = "us-east-1a"
} 
